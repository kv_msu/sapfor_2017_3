#include "private_arrays_resizing.h"
#include "../GraphLoop/graph_loops.h"
#include "../Utils/SgUtils.h"
#include "../Utils/utils.h"
#include "../Utils/errors.h"
#include "../DirectiveProcessing/directive_parser.h"
#include "../LoopAnalyzer/loop_analyzer.h"

#include <set>
#include <queue>
#include <vector>

using std::string;
using std::set;
using std::map;
using std::queue;
using std::vector;

static void printExp(SgExpression *exp)
{
    queue<SgExpression*> qq;
    qq.push(exp);
    printf("Expression: ");
    
    while (!qq.empty())
    {
        SgExpression *exp = qq.front();
        qq.pop();

        printf("[%s]: %s\n", tag[exp->variant()], exp->unparse());
        if (exp->variant() == TYPE_OP)
            if (exp->symbol())
                printf("%s, %s", exp->symbol()->identifier(), tag[exp->symbol()->variant()]);

        if (exp->lhs())
            qq.push(exp->lhs());
        if (exp->rhs())
            qq.push(exp->rhs());
    }
}

static void printSt(SgStatement *st)
{
    printf("Statement [%s]: %s\n", tag[st->variant()], st->unparse());
    queue<SgExpression*> qq;
    for (int i = 0; i < 3; ++i)
    {
        if (st->expr(i))
        {
            printf("[%d,%s]: %s\n", i, tag[st->expr(i)->variant()], st->expr(i)->unparse());
            qq.push(st->expr(i));
        }
    }
    while (!qq.empty())
    {
        SgExpression *exp = qq.front();
        qq.pop();

        printf("[%s]: %s\n", tag[exp->variant()], exp->unparse());
        if (exp->variant() == TYPE_OP)
            if (exp->symbol())
                printf("%s, %s", exp->symbol()->identifier(), tag[exp->symbol()->variant()]);

        if (exp->lhs())
            qq.push(exp->lhs());
        if (exp->rhs())
            qq.push(exp->rhs());
    }
}

static SgExpression* findSymbolInExprList(SgSymbol* symbol, SgExpression* list)
{
    while (list)
    {
        if (list->variant() != EXPR_LIST || list->lhs()->symbol() == NULL)
            return NULL;

        if (isEqSymbols(list->lhs()->symbol(), symbol))
            return list->lhs();

        list = list->rhs();
    }

    return NULL;
}

static char* constructNewArrayName(const char *oldName, bool isExpansion)
{
    string name(oldName), newName;
    name += isExpansion ? "_ex" : "_sh";
    newName = name + std::to_string(1);
    for (int n = 2; ifSymbolExists(current_file, newName); ++n)
        newName = name + std::to_string(n);

    char* newNameChar = (char*)malloc((newName.size() + 1) * sizeof(char));
    addToCollection(__LINE__, __FILE__, newNameChar, 0);
    if (newNameChar)
        strcpy(newNameChar, newName.c_str());
    else
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

    return newNameChar;
}

static const string constructNewBoundName(const char *oldName, int loopLineNumber, const char *add)
{
    string name(oldName), newName;
    name += "_loop_" + std::to_string(loopLineNumber);
    name += add;// start ? "_start" : "_end";
    newName = name;
    for (int n = 0; ifSymbolExists(current_file, newName); ++n)
        newName = name + "_" + std::to_string(n);

    return newName;
}

static SgSymbol* createNewArrayNameSymbol(SgExpression* declaration, bool isExpansion)
{
    SgType* type = new SgType(T_ARRAY);
    char* newNameStr = constructNewArrayName(declaration->symbol()->identifier(), isExpansion);
    SgSymbol* newName = new SgSymbol(VARIABLE_NAME, newNameStr, type, NULL);
    return newName;
}

static SgExpression* getExtensionPartOfArray(LoopGraph* forLoop, int depthOfResize)
{
    vector<SgExpression*> dimensions(depthOfResize);
    LoopGraph* curLoop = forLoop;
    for (int i = 0; i < depthOfResize; ++i)
    {
        SgForStmt* loopStmt = (SgForStmt*)(curLoop->loop->GetOriginal());
        dimensions[depthOfResize - 1 - i] = new SgExpression(DDOT, loopStmt->start()->copyPtr(), loopStmt->end()->copyPtr(), NULL);
        if (i + 1 != depthOfResize)
            curLoop = curLoop->children[0];
    }

    SgExpression* newTail = NULL;
    for (int i = dimensions.size() - 1; i >= 0; --i)
        newTail = new SgExpression(EXPR_LIST, dimensions[i], newTail, (SgSymbol*)NULL);

    return newTail;
}

static void extendArrayDeclaration(LoopGraph* forLoop, const vector<int>& dimensions, SgExpression*& exprToExtend, SgSymbol* newArraySym)
{
    exprToExtend->setSymbol(newArraySym);
    
    bool canBeStatic = true;
    for (int i = 0; i < dimensions.size(); ++i)
        if (dimensions[i] == 0)
            canBeStatic = false;
    
    SgExpression* newTail = NULL;
    if (canBeStatic)
        newTail = getExtensionPartOfArray(forLoop, dimensions.size());
    else
    {
        for (int i = dimensions.size() - 1; i >= 0; --i)
        {
            SgExpression* resizedIndex = new SgExpression(DDOT);
            newTail = new SgExpression(EXPR_LIST, resizedIndex, newTail, (SgSymbol*)NULL);
        }
    }

    SgExpression* oldTail = exprToExtend->lhs();
    if (oldTail == NULL) // create array from scalar
    {
        exprToExtend = new SgArrayRefExp(*newArraySym);
        exprToExtend->setLhs(newTail);
    }
    else
    {
        while (oldTail->rhs() != NULL)
            oldTail = oldTail->rhs();

        oldTail->setRhs(newTail);
    }
}

static void reduceArray(const vector<int> &indexes, SgExpression *expressionToReduce, SgSymbol *newSymbol)
{
    expressionToReduce->setSymbol(newSymbol);

    vector<SgExpression*> tailVec(0);
    SgExpression *tail = expressionToReduce->lhs();
    for (int i = 0; i < indexes.size(); ++i)
    {
        if (indexes[i] == 0)
            tailVec.push_back(tail->lhs());

        tail = tail->rhs();
    }

    std::reverse(tailVec.begin(), tailVec.end());
    SgExpression *list = makeExprList(tailVec, false);

    expressionToReduce->setLhs(list);
}

static SgExpression* extendArrayRef(const vector<SgSymbol*>& indexes, SgExpression *expressionToExtend,
                                    SgSymbol *newSymbol, const vector<SgExpression*> &lowBounds)
{
    SgExpression *newTail = NULL;
    for (int i = indexes.size() - 1; i >= 0; --i)
    {
        SgExpression *resizedIndex = new SgVarRefExp(indexes[i]);
        newTail = new SgExpression(EXPR_LIST, resizedIndex, newTail, (SgSymbol*)NULL);
    }

    SgExpression *oldTail = expressionToExtend->lhs();
    if (oldTail == NULL) 
    {
        if (expressionToExtend->variant() == VAR_REF) // create array from scalar
        {
            expressionToExtend = new SgArrayRefExp(*newSymbol);
            expressionToExtend->setLhs(newTail);
        }
        else if (isArrayRef(expressionToExtend)) // create array from full array ref
        {
            SgArrayRefExp *curr = (SgArrayRefExp*)expressionToExtend;
            expressionToExtend->setSymbol(newSymbol);
            for (auto &elem : lowBounds) // add low bounds
                curr->addSubscript(*elem);
            curr->addSubscript(*newTail);
        }
        else
            printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
    }
    else
    {
        while (oldTail->rhs() != NULL)
            oldTail = oldTail->rhs();

        oldTail->setRhs(newTail);
        expressionToExtend->setSymbol(newSymbol);
    }

    return expressionToExtend;
}

static bool isAllocatable(SgSymbol* symbol)
{
    return (symbol->attributes() & ALLOCATABLE_BIT) != 0;
}

static bool isAllocatable(SgStatement *decl)
{
    SgExpression *params = decl->expr(2);
    while (params != NULL)
    {
        if (params->lhs()->variant() == ALLOCATABLE_OP)
            return true;
        params = params->rhs();
    }

    return false;
}

static void setAllocatable(SgStatement *newDecl, SgSymbol *origSymbol)
{
    if (isAllocatable(origSymbol))
        return;

    SgExpression *params = newDecl->expr(2);
    SgExpression newParams (EXPR_LIST, new SgExpression(ALLOCATABLE_OP), params, (SgSymbol*)NULL);
    newDecl->setExpression(2, newParams);
    
    //объявление с DIMENSION или с ALLOCATABLBE
    if (newDecl->variant() != VAR_DECL)
    {
        newDecl->setVariant(VAR_DECL);
        newDecl->setExpression(1, *(new SgTypeExp(*origSymbol->type()->baseType())));
    }
}

static SgSymbol* alterExtendArrayDeclaration(LoopGraph* forLoop, SgStatement* declarationStmt, SgSymbol* arraySymbol, vector<int>& dimensions)
{
    SgSymbol* newArraySymbol = NULL;
    SgExpression* array = findSymbolInExprList(arraySymbol, declarationStmt->expr(0));

    SgExpression* newArray = array->copyPtr();
    newArraySymbol = createNewArrayNameSymbol(newArray, true);

    extendArrayDeclaration(forLoop, dimensions, newArray, newArraySymbol);

    SgExpression newExprList(EXPR_LIST, newArray, (SgExpression*)NULL, (SgSymbol*)NULL);
    declarationStmt->setExpression(0, newExprList);

    bool canBeStatic = true;
    for (int i = 0; i < dimensions.size(); ++i)
        if (dimensions[i] == 0)
            canBeStatic = false;
    
    if (!canBeStatic)
    {
        setAllocatable(declarationStmt, arraySymbol);
        newArraySymbol->setAttribute(ALLOCATABLE_BIT);
    }

    if (isAllocatable(newArraySymbol))
    {
        SgExpression* decl = findSymbolInExprList(newArraySymbol, declarationStmt->expr(0));
        SgExpression* list = decl->lhs();
        while (list)
        {
            list->setLhs(new SgExpression(DDOT));
            list = list->rhs();
        }
    }

    if (newArraySymbol == NULL)
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

    return newArraySymbol;
}

static SgSymbol* alterShrinkArrayDeclaration(SgStatement *declarationStatement, SgSymbol *arraySymbol, vector<int> &dimensions)
{
    SgSymbol *newArraySymbol = NULL;
    SgExpression *array = findSymbolInExprList(arraySymbol, declarationStatement->expr(0));

    SgExpression *newArray = array->copyPtr();
    newArraySymbol = createNewArrayNameSymbol(newArray, false);

    reduceArray(dimensions, newArray, newArraySymbol);

    SgExpression newExprList(EXPR_LIST, newArray, (SgExpression*)NULL, (SgSymbol*)NULL);
    declarationStatement->setExpression(0, newExprList);
        
    if (newArraySymbol == NULL)
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

    return newArraySymbol;
}

static void extendArrayRefsInExpr(const vector<SgSymbol*> &indexes, SgExpression *expr, SgSymbol *arraySymbol,
                                  SgSymbol *newArraySymbol, const vector<SgExpression*> &lowBounds)
{
    SgExpression *lhs = expr->lhs(), *rhs = expr->rhs();

    if (lhs)
    {
        if (isArrayRef(lhs) && isEqSymbols(arraySymbol, lhs->symbol()))
            extendArrayRef(indexes, lhs, newArraySymbol, lowBounds);
        else if (lhs->variant() == VAR_REF && isEqSymbols(arraySymbol, lhs->symbol()))
        {
            SgExpression* extended = extendArrayRef(indexes, lhs, newArraySymbol, lowBounds);
            expr->setLhs(extended);
        }
        else
            extendArrayRefsInExpr(indexes, lhs, arraySymbol, newArraySymbol, lowBounds);
    }

    if (rhs)
    {
        if (isArrayRef(rhs) && isEqSymbols(arraySymbol, rhs->symbol()))
            extendArrayRef(indexes, rhs, newArraySymbol, lowBounds);
        else if (rhs->variant() == VAR_REF && isEqSymbols(arraySymbol, rhs->symbol()))
        {
            SgExpression* extended = extendArrayRef(indexes, rhs, newArraySymbol, lowBounds);
            expr->setRhs(extended);
        }
        else
            extendArrayRefsInExpr(indexes, rhs, arraySymbol, newArraySymbol, lowBounds);
    }
}

static void extendArrayRefs(const vector<SgSymbol*> &indexes, SgStatement *st, SgSymbol *arraySymbol, 
                            SgSymbol *newArraySymbol, const vector<SgExpression*> &lowBounds)
{
    for (int i = 0; i < 3; ++i)
    {
        if (st->expr(i))
        {
            if (isArrayRef(st->expr(i)) && isEqSymbols(arraySymbol, st->expr(i)->symbol()))
                extendArrayRef(indexes, st->expr(i), newArraySymbol, lowBounds);
            else if (st->expr(i)->variant() == VAR_REF && isEqSymbols(arraySymbol, st->expr(i)->symbol()))
            {
                SgExpression* extended = extendArrayRef(indexes, st->expr(i), newArraySymbol, lowBounds);
                st->setExpression(i, *extended);
            }
            else
                extendArrayRefsInExpr(indexes, st->expr(i), arraySymbol, newArraySymbol, lowBounds);
        }
    }
}

static SgStatement* createNewDeclarationStatemnet(SgStatement *loop, SgStatement *originalDeclaration, SgSymbol *arraySymbol)
{
    SgStatement *lastDecl = getFuncStat(loop);
    while (lastDecl)
    {
        if (isSgExecutableStatement(lastDecl))
            break;
        lastDecl = lastDecl->lexNext();
    }

    SgExpression *exprList = originalDeclaration->expr(0);
    while (!isEqSymbols(exprList->lhs()->symbol(), arraySymbol))
        exprList = exprList->rhs();

    SgExpression newExprList(EXPR_LIST, exprList->lhs(), NULL, NULL);
    SgStatement *newDeclaration = originalDeclaration->copyPtr();

    newDeclaration->setExpression(0, newExprList);
    lastDecl->insertStmtBefore(*newDeclaration, *lastDecl->controlParent());

    return newDeclaration;
}

static SgExpression* constructBoundCall(bool upBound, SgSymbol *array, int dim)
{
    const char *boundName = NULL;
    upBound ? boundName = "ubound" : boundName = "lbound";

    SgSymbol boundS = SgSymbol(FUNCTION_NAME, boundName);

    SgExpression *dimParam = new SgExpression(EXPR_LIST, new SgValueExp(dim), NULL, NULL);
    SgExpression params(EXPR_LIST, new SgExpression(ARRAY_REF, NULL, NULL, array), dimParam, NULL);

    return new SgFunctionCallExp(boundS, params);
}

static SgExpression* constructReducedAllocation(SgExpression *origArray, SgSymbol *arraySymbol, vector<int> *indexes)
{
    SgExpression *arrayRef = origArray->copyPtr();
    reduceArray(*indexes, arrayRef, arraySymbol);

    return new SgExpression(EXPR_LIST, arrayRef, (SgExpression*)NULL, (SgSymbol*)NULL);
}

static SgStatement* getAllocationStmt(LoopGraph* loop, SgSymbol* symbol)
{
    SgStatement* originalDeclaration = declaratedInStmt(symbol);

    if (isAllocatable(symbol))
    {
        SgForStmt* loopStmt = (SgForStmt*)(loop->loop->GetOriginal());

        SgStatement* allocationStmt = NULL;
        for (auto& alloc : getAttributes<SgStatement*, SgStatement*>(originalDeclaration, set<int>{ ALLOCATE_STMT }))
        {
            if (alloc->variant() != ALLOCATE_STMT)
                continue;

            if (NULL == findSymbolInExprList(symbol, alloc->expr(0)))
                continue;

            if (allocationStmt == NULL)
                allocationStmt = alloc;
            else
                if (allocationStmt->lineNumber() < alloc->lineNumber() && alloc->lineNumber() < loopStmt->lineNumber())
                    allocationStmt = alloc;
        }

        return allocationStmt;
    }
    else
        return NULL;
}

static void fillLowBounds(LoopGraph* forLoop, SgSymbol* origArraySymbol, SgStatement* originalDeclaration, vector<SgExpression*>& lowBounds)
{
    SgExpression* origArray = NULL;

    if (isAllocatable(origArraySymbol))
    {
        SgStatement* allocationStmt = getAllocationStmt(forLoop, origArraySymbol);
        origArray = findSymbolInExprList(origArraySymbol, allocationStmt->expr(0));
    }
    else
        origArray = findSymbolInExprList(origArraySymbol, originalDeclaration->expr(0));

    SgExpression* arrayRef = origArray->copyPtr();
    SgExpression* oldTail = arrayRef->lhs();
    while (oldTail != NULL)
    {
        if (oldTail->lhs()->variant() == DDOT)
            lowBounds.push_back(oldTail->lhs()->lhs());
        else
            lowBounds.push_back(new SgValueExp(1));

        oldTail = oldTail->rhs();
    }
}

static SgExpression* constructArrayAllocationExp(LoopGraph* forLoop, SgExpression *origArray, SgSymbol *arraySymbol, int depthOfResize)
{
    SgExpression *arrayRef = origArray->copyPtr();
    arrayRef->setSymbol(arraySymbol);
    vector<SgExpression*> dimensions(depthOfResize);

    SgExpression *newTail = getExtensionPartOfArray(forLoop, depthOfResize);

    SgExpression *oldTail = arrayRef->lhs();
    SgSymbol *origArraySymbol = origArray->symbol();
    int curDim = 0;
    SgExpression *attachTo = oldTail;
    while (oldTail != NULL)
    {
        curDim++;
        if (oldTail->lhs()->variant() == DDOT)
        {
            SgExpression *ddot = oldTail->lhs();
            if (!ddot->lhs())
                ddot->setLhs(constructBoundCall(false, origArraySymbol, curDim));
            if (!ddot->rhs())
                ddot->setRhs(constructBoundCall(true, origArraySymbol, curDim));
        }

        oldTail = oldTail->rhs();
        if (oldTail)
            attachTo = oldTail;
    }

    if (arrayRef->variant() == VAR_REF)// create array from scalar
    {
        arrayRef = new SgArrayRefExp(*arrayRef->symbol());
        arrayRef->setLhs(newTail);
    }
    else
        attachTo->setRhs(newTail);

    return new SgExpression(EXPR_LIST, arrayRef, (SgExpression*)NULL, (SgSymbol*)NULL);
}

static void insertAllocDealloc(LoopGraph* forLoop, SgSymbol* origArraySymbol, SgSymbol* arraySymbol, int depthOfResize, vector<int>* indexes = NULL)
{
    SgForStmt *loopStmt = (SgForStmt*)(forLoop->loop->GetOriginal());
    vector<SgExpression*> lowBounds;

    SgStatement *originalDeclaration = declaratedInStmt(origArraySymbol);
    SgExpression *origArray = NULL;

    if (isAllocatable(origArraySymbol))
    {
        SgStatement *allocationStmt = getAllocationStmt(forLoop, origArraySymbol);
        origArray = findSymbolInExprList(origArraySymbol, allocationStmt->expr(0));
    }
    else
        origArray = findSymbolInExprList(origArraySymbol, originalDeclaration->expr(0));

    SgExpression* arrayAllocation = indexes == NULL
        ? constructArrayAllocationExp(forLoop, origArray, arraySymbol, depthOfResize)
        : constructReducedAllocation(origArray, arraySymbol, indexes);
    SgExpression *arrayDeallocation = new SgExpression(EXPR_LIST,
        new SgExpression(ARRAY_REF, (SgExpression*)NULL, (SgExpression*)NULL, arraySymbol),
        (SgExpression*)NULL, (SgSymbol*)NULL);

    SgStatement *allocate = new SgStatement(ALLOCATE_STMT, (SgLabel*)NULL, (SgSymbol*)NULL, arrayAllocation, (SgExpression*)NULL, (SgExpression*)NULL);
    SgStatement *deallocate = new SgStatement(DEALLOCATE_STMT, (SgLabel*)NULL, (SgSymbol*)NULL, arrayDeallocation, (SgExpression*)NULL, (SgExpression*)NULL);

    loopStmt->insertStmtBefore(*allocate, *loopStmt->controlParent());
    loopStmt->lastNodeOfStmt()->insertStmtAfter(*deallocate, *loopStmt->controlParent());
}

static bool containsFunctionCall(SgExpression* exp) 
{
    bool retVal = false;

    if (exp)
    {
        if (exp->variant() == FUNC_CALL)
            retVal = true;

        if (exp->lhs())
            retVal = retVal || containsFunctionCall(exp->lhs());
        if (exp->rhs())
            retVal = retVal || containsFunctionCall(exp->rhs());
    }
    return retVal;
}

static SgSymbol* createVarDeclaration(SgForStmt *loopStmt, const char *add)
{
    SgSymbol *doName = loopStmt->doName();
    string newNameStr = constructNewBoundName(doName->identifier(), loopStmt->lineNumber(), add);
    SgSymbol *newName = new SgSymbol(VARIABLE_NAME, newNameStr.c_str(), doName->type(), NULL);

    SgStatement *decl = declaratedInStmt(doName);

    SgStatement *newDecl = decl->copyPtr();
    newDecl->setExpression(0, new SgExpression(EXPR_LIST, new SgVarRefExp(newName), NULL, NULL));
    decl->insertStmtAfter(*newDecl, *decl->controlParent());

    return newName;
}

static SgStatement* createBoundVariableAssign(SgSymbol *varName, SgExpression *value)
{
    SgAssignStmt *assign = new SgAssignStmt(*new SgVarRefExp(varName), *value);
    return assign;
}

static void replaceFunctionBounds(SgExpression *ex, SgForStmt *loopStmt, SgForStmt *topLoopStmt, const string &kind)
{
    if (containsFunctionCall(ex))
    {
        SgSymbol *newStart = createVarDeclaration(loopStmt, kind.c_str());
        SgStatement *assignment = createBoundVariableAssign(newStart, ex);

        topLoopStmt->insertStmtBefore(*assignment, *topLoopStmt->controlParent());
        if (kind == "_start")
            loopStmt->setStart(*assignment->expr(0));
        if (kind == "_end")
            loopStmt->setEnd(*assignment->expr(0));
        if (kind == "_step")
            loopStmt->setStep(*assignment->expr(0));
    }
}

static void replaceFunctionBounds(LoopGraph *forLoop, int depthOfResize)
{
    LoopGraph *curLoop = forLoop;
    SgForStmt *topLoopStmt = (SgForStmt*) (curLoop->loop->GetOriginal());
    for (int i = 0; i < depthOfResize; ++i)
    {
        SgForStmt *loopStmt = (SgForStmt*) (curLoop->loop->GetOriginal());
        
        replaceFunctionBounds(loopStmt->start(), loopStmt, topLoopStmt, "_start");
        replaceFunctionBounds(loopStmt->end(), loopStmt, topLoopStmt, "_end");
        replaceFunctionBounds(loopStmt->step(), loopStmt, topLoopStmt, "_step");
        
        if (i + 1 != depthOfResize)
            curLoop = curLoop->children[0];
    }
}

static SgSymbol* createReducedToVariableArray(SgStatement *copiedDeclaration, int lineNumber, SgSymbol *arraySymbol)
{
    SgType *type = copiedDeclaration->expr(1)->type();
    string newNameStr = constructNewBoundName(arraySymbol->identifier(), lineNumber, "_reduced");
    SgSymbol *newName = new SgSymbol(VARIABLE_NAME, newNameStr.c_str(), type, NULL);

    copiedDeclaration->setExpression(0, new SgExpression(EXPR_LIST, new SgVarRefExp(newName), NULL, NULL));
    return newName;
}

static void reduceArrayRefsInExpr(SgExpression *expr, SgSymbol *arraySymbol, SgSymbol *newSymbol, bool reduceToVariable, vector<int> &indexes)
{
    if (expr == NULL)
        return;

    SgExpression *lhs = expr->lhs(), *rhs = expr->rhs();
    
    if (lhs)
    {
        if (isArrayRef(lhs) && isEqSymbols(arraySymbol, lhs->symbol()))
        {
            if (reduceToVariable)
                expr->setLhs(new SgVarRefExp(newSymbol));
            else
                reduceArray(indexes, lhs, newSymbol);
        }
        else
            reduceArrayRefsInExpr(lhs, arraySymbol, newSymbol, reduceToVariable, indexes);
    }

    if (rhs)
    {
        if (isArrayRef(rhs) && isEqSymbols(arraySymbol, rhs->symbol()))
        {
            if (reduceToVariable)
                expr->setRhs(new SgVarRefExp(newSymbol));
            else
                reduceArray(indexes, rhs, newSymbol);
        }
        else
            reduceArrayRefsInExpr(rhs, arraySymbol, newSymbol, reduceToVariable, indexes);
    }
}

static void reduceArrayRefs(SgStatement *st, SgSymbol *arraySymbol, SgSymbol *newSymbol, bool reduceToVariable, vector<int> &indexes)
{
    for (int i = 0; i < 3; ++i)
    {
        if (st->expr(i))
        {
            SgExpression* expr = st->expr(i);
            if (isArrayRef(expr) && isEqSymbols(arraySymbol, expr->symbol()))
            {
                if (reduceToVariable)
                    st->setExpression(i, new SgVarRefExp(newSymbol));
                else
                    reduceArray(indexes, expr, newSymbol);
            }
            else
                reduceArrayRefsInExpr(expr, arraySymbol, newSymbol, reduceToVariable, indexes);
        }
    }
}

static vector<int> fillIndexes(SgSymbol *arr, SgExprListExp *listExp)
{
    vector<int> indexes(0);

    SgExpression* sym = findSymbolInExprList(arr, listExp);    
    if (sym)
    {
        SgExpression* expr = sym->lhs();
        while (expr)
        {
            indexes.push_back(expr->lhs()->valueInteger());
            expr = expr->rhs();
        }
    }

    return indexes;
}

static void renamePrivateVarsInAttributes(std::vector<SgStatement*>& attrs, map<SgSymbol*, SgSymbol*>& symbols)
{
    for (SgStatement* st : attrs)
    {
        if (st->variant() != SPF_ANALYSIS_DIR)
            return;

        SgExpression* exprList = st->expr(0);
        while (exprList)
        {
            if (exprList->lhs()->variant() == ACC_PRIVATE_OP)
            {
                SgExpression* list = exprList->lhs()->lhs();
                while (list)
                {
                    if (list->lhs()->variant() == ARRAY_REF)
                        for (auto& pair : symbols)
                            if (isEqSymbols(pair.first, list->lhs()->symbol()))
                                list->setLhs(new SgExpression(ARRAY_REF, NULL, NULL, pair.second));

                    list = list->rhs();
                }
            }
            exprList = exprList->rhs();
        }
    }
}

static SgSymbol* shrinkArray(LoopGraph *forLoop, SgSymbol *arraySymbol, vector<int> &indexes)
{
    int maxDepth = forLoop->perfectLoop;
    LoopGraph *curLoop = forLoop;

    bool reduceToVariable = true;
    for (int i = 0; i < indexes.size(); ++i)
    {
        if (indexes[i] == 0)
        {
            reduceToVariable = false;
            break;
        }
    }

    for (int i = 0; i < maxDepth; ++i)
    {
        if (curLoop->children.size() == 1)
            curLoop = curLoop->children[0];
        else if (i != maxDepth - 1)
        {
            printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
            return NULL;
        }
    }

    replaceFunctionBounds(forLoop, maxDepth);
    SgStatement *originalDeclaration = declaratedInStmt(arraySymbol);
    SgStatement *copiedDeclaration = createNewDeclarationStatemnet(forLoop->loop->GetOriginal(), originalDeclaration, arraySymbol);

    SgSymbol *newSymbol = reduceToVariable
            ? createReducedToVariableArray(copiedDeclaration, forLoop->lineNum, arraySymbol)
            : alterShrinkArrayDeclaration(copiedDeclaration, arraySymbol, indexes);

    if (newSymbol)
    {
        SgForStmt *loopStmt = (SgForStmt*)(forLoop->loop->GetOriginal());

        if (!reduceToVariable && isAllocatable(arraySymbol))
            insertAllocDealloc(forLoop, arraySymbol, newSymbol, indexes.size(), &indexes);

        for (SgStatement *st = loopStmt->lexNext(); st != loopStmt->lastNodeOfStmt()->lexNext(); st = st->lexNext())
            if (st->variant() != ALLOCATE_STMT && st->variant() != DEALLOCATE_STMT)
                reduceArrayRefs(st, arraySymbol, newSymbol, reduceToVariable, indexes);

        return newSymbol;
    }

    return NULL;
}

static SgSymbol* resizeArray(LoopGraph *forLoop, SgSymbol *arraySymbol, int depthOfResize)
{
    if (depthOfResize < 0 || depthOfResize > forLoop->perfectLoop)
        depthOfResize = forLoop->perfectLoop;

    vector<int> dimensions(depthOfResize);
    vector<SgSymbol*> indexes(depthOfResize);

    LoopGraph *curLoop = forLoop;
    bool canBeStatic = true;
    for (int i = 0; i < depthOfResize; ++i)
    {
        SgForStmt *loopStmt = (SgForStmt*)(curLoop->loop->GetOriginal());
        dimensions[depthOfResize - 1 - i] = curLoop->calculatedCountOfIters;
        if (dimensions[depthOfResize - 1 - i] == 0)
            canBeStatic = false;
        indexes[depthOfResize - 1 - i] = loopStmt->doName();

        if (curLoop->children.size() == 1)
            curLoop = curLoop->children[0];
        else if (i != depthOfResize - 1)
        {
            printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
            return NULL;
        }
    }

    replaceFunctionBounds(forLoop, depthOfResize);

    SgStatement *originalDeclaration = declaratedInStmt(arraySymbol);
    SgStatement *copiedOriginalDecl = createNewDeclarationStatemnet(forLoop->loop->GetOriginal(), originalDeclaration, arraySymbol);
    SgSymbol *newArraySymbol = alterExtendArrayDeclaration(forLoop, copiedOriginalDecl, arraySymbol, dimensions);

    vector<SgExpression*> lowBounds(depthOfResize);
    fillLowBounds(forLoop, arraySymbol, originalDeclaration, lowBounds);

    if (newArraySymbol)
    {
        SgForStmt *loopStmt = (SgForStmt*)(forLoop->loop->GetOriginal());
        if (!canBeStatic || isAllocatable(newArraySymbol))
            insertAllocDealloc(forLoop, arraySymbol, newArraySymbol, depthOfResize);
        for (SgStatement *st = loopStmt->lexNext(); st != loopStmt->lastNodeOfStmt()->lexNext(); st = st->lexNext())
            if (st->variant() != ALLOCATE_STMT && st->variant() != DEALLOCATE_STMT)
                extendArrayRefs(indexes, st, arraySymbol, newArraySymbol, lowBounds);
        
        return newArraySymbol;
    }

    return NULL;
}

//for testing
static SgSymbol* findSymbol(LoopGraph *forLoop, const char *arrayName)
{
    SgForStmt *loopStmt = (SgForStmt*)forLoop->loop->GetOriginal();
    if (loopStmt)
    {
        for (SgStatement *st = loopStmt->lexNext(); st != loopStmt->lastNodeOfStmt()->lexNext(); st = st->lexNext())
        {
            queue<SgExpression*> toCheck = queue<SgExpression*>();

            for (int i = 0; i < 3; ++i)
                if (st->expr(i))
                    toCheck.push(st->expr(i));

            while (!toCheck.empty())
            {
                SgExpression *curExp = toCheck.front();
                toCheck.pop();

                if ((isArrayRef(curExp) || curExp->variant() == VAR_REF) && !strcmp(arrayName, curExp->symbol()->identifier()))
                    return curExp->symbol();
                else
                {
                    if (curExp->lhs())
                        toCheck.push(curExp->lhs());
                    if (curExp->rhs())
                        toCheck.push(curExp->rhs());
                }
            }
        }
    }
    return NULL;
}

//Вычислять размер массива с учётом шага цикла - //TODO
int privateArraysResizing(SgFile *file, std::vector<LoopGraph*> &loopGraphs, const set<SgSymbol*> &doForThisPrivates, vector<Messages> &messages)
{
    map<int, LoopGraph*> mapLoopGraph;
    createMapLoopGraph(loopGraphs, mapLoopGraph);

    for (auto &loopPair : mapLoopGraph)
    {
        LoopGraph *loop = loopPair.second;
        auto attrsTr = getAttributes<SgStatement*, SgStatement*>(loop->loop->GetOriginal(), set<int>{ SPF_TRANSFORM_DIR });
        auto attrsPriv = getAttributes<SgStatement*, SgStatement*>(loop->loop->GetOriginal(), set<int>{ SPF_ANALYSIS_DIR });
        
        set<Symbol*> privates;
        set<SgSymbol*> arrayPrivates;
        for (auto &spf : attrsPriv)
            fillPrivatesFromComment(new Statement(spf), privates);
        for (auto &s : privates)
            if (s->type()->variant() == T_ARRAY)
                arrayPrivates.insert(s->GetOriginal());

        arrayPrivates.insert(doForThisPrivates.begin(), doForThisPrivates.end());

        if (arrayPrivates.size() == 0)
        {
            string str;
            __spf_printToBuf(str, "Can not do PRIVATE EXPANSION for this loop - privates not found");

            //messages.push_back(Messages(NOTE, loop->lineNum, str, 2008));
            __spf_print(1, "%s on line %d\n", str.c_str(), loop->lineNum);
        }
        else
        {
            for (auto attr : attrsTr)
            {
                SgExpression *list = attr->expr(0);
                vector<SgExpression*> newL;
                map<SgSymbol*, SgSymbol*> symbols;

                while (list)
                {
                    symbols.clear();

                    if (list->lhs()->variant() == SPF_EXPAND_OP)
                    {
                        int deep = -1;
                        if (list->lhs()->lhs() != NULL)
                        {
                            SgExprListExp* listExp = isSgExprListExp(list->lhs()->lhs());
                            checkNull(listExp, convertFileName(__FILE__).c_str(), __LINE__);
                            deep = listExp->length();
                        }

                        for (SgSymbol* privArr : arrayPrivates)
                        {
                            SgSymbol* newSymbol = resizeArray(loop, privArr, deep);
                            if (newSymbol)
                                symbols.insert(std::make_pair(privArr, newSymbol));
                        }
                        renamePrivateVarsInAttributes(attrsPriv, symbols);
                    }
                    else if (list->lhs()->variant() == SPF_SHRINK_OP)
                    {
                        SgExprListExp* listExp = isSgExprListExp(list->lhs()->lhs());
                        checkNull(listExp, convertFileName(__FILE__).c_str(), __LINE__);

                        for (SgSymbol* privArr : arrayPrivates)
                        {
                            vector<int> indexes = fillIndexes(privArr, listExp);
                            if (!indexes.empty())
                            {
                                SgSymbol* newSymbol = shrinkArray(loop, privArr, indexes);
                                if (newSymbol)
                                    symbols.insert(std::make_pair(privArr, newSymbol));
                            }
                        }
                        renamePrivateVarsInAttributes(attrsPriv, symbols);
                    }
                    else
                        newL.push_back(list);

                    list = list->rhs();
                }

                SgExpression *ex = NULL;
                SgExpression *p = NULL;
                for (int z = 0; z < newL.size(); ++z)
                {
                    if (z == 0)
                        p = ex = newL[z];
                    else
                    {
                        ex->setRhs(newL[z]);
                        ex = ex->rhs();
                    }
                }
                attr->setExpression(0, p);
            }
        }
    }

    return 0;
}
